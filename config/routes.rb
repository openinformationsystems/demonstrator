Rails.application.routes.draw do
  root 'welcome#index'
  get 'welcome/index'

  resources :cuisines
  resources :recipe_categories
  resources :shop_franchises

  resources :products do
  	resources :product_nutritions, controller: 'nutrition'
    resources :product_shops
    resources :product_in_shop_franchises
  end

  get '/products/:id/shop_availability', to: 'products#show_shop_availability'

  resources :recipes do
  	resources :recipe_steps
  	resources :recipe_ingredients
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
