class AddWebsiteToShopFranchise < ActiveRecord::Migration[5.0]
  def change
  	add_column :shop_franchises, :website, :string
  end
end
