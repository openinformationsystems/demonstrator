class CreateRecipeSteps < ActiveRecord::Migration[5.0]
  def change
    create_table :recipe_steps do |t|
      t.integer :step_number
      t.text :description
      t.string :image
      t.references :recipe, foreign_key: true

      t.timestamps
    end
  end
end
