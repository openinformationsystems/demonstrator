class CreateRecipeIngredients < ActiveRecord::Migration[5.0]
  def change
    create_table :recipe_ingredients do |t|
      t.references :recipe, foreign_key: true
      t.string :quantity
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
