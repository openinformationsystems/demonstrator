class AddCategoryToRecipe < ActiveRecord::Migration[5.0]
  def change
  	add_reference :recipes, :recipe_category, foreign_key: true
  end
end
