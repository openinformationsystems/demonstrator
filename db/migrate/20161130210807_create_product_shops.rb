class CreateProductShops < ActiveRecord::Migration[5.0]
  def change
    create_table :product_shops do |t|
      t.references :product, foreign_key: true
      t.string :barcode
      t.decimal :amount
      t.text :amount_unit

      t.timestamps
    end
  end
end
