class CreateRecipes < ActiveRecord::Migration[5.0]
  def change
    create_table :recipes do |t|
      t.string :name
      t.text :description
      t.string :image
      t.references :product, foreign_key: true
      t.references :cuisine, foreign_key: true
      t.string :cooking_method
      t.string :cooking_time
      t.string :preperation_time
      t.string :amount_of_servings

      t.timestamps
    end
  end
end
