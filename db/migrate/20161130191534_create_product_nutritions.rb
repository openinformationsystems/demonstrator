class CreateProductNutritions < ActiveRecord::Migration[5.0]
  def change
    create_table :product_nutritions do |t|
      t.references :product, foreign_key: true
      t.decimal :calories
      t.decimal :fats
      t.decimal :saturated_fats
      t.decimal :carbohydrates
      t.decimal :sugars
      t.decimal :fiber
      t.decimal :proteins
      t.decimal :salt
      t.decimal :vitamin_a
      t.decimal :vitamin_b1
      t.decimal :vitamin_b2
      t.decimal :vitamin_b3
      t.decimal :vitamin_b5
      t.decimal :vitamin_b6
      t.decimal :vitamin_b7
      t.decimal :vitamin_b9
      t.decimal :vitamin_b12
      t.decimal :vitamin_c
      t.decimal :vitamin_d
      t.decimal :vitamin_e
      t.decimal :vitamin_k
      t.decimal :nutrition_per
      t.string :nutrition_per_unit

      t.timestamps
    end
  end
end
