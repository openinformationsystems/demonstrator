class CreateShopFranchises < ActiveRecord::Migration[5.0]
  def change
    create_table :shop_franchises do |t|
      t.string :name
      t.text :description
      t.string :image

      t.timestamps
    end
  end
end
