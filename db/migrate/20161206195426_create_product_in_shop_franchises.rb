class CreateProductInShopFranchises < ActiveRecord::Migration[5.0]
  def change
    create_table :product_in_shop_franchises do |t|
      t.references :product, foreign_key: true
      t.references :shop_franchise, foreign_key: true
      t.decimal :price
      t.date :price_date

      t.timestamps
    end
  end
end
