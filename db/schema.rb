# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161207181931) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cuisines", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_in_shop_franchises", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "shop_franchise_id"
    t.decimal  "price"
    t.date     "price_date"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["product_id"], name: "index_product_in_shop_franchises_on_product_id", using: :btree
    t.index ["shop_franchise_id"], name: "index_product_in_shop_franchises_on_shop_franchise_id", using: :btree
  end

  create_table "product_nutritions", force: :cascade do |t|
    t.integer  "product_id"
    t.decimal  "calories"
    t.decimal  "fats"
    t.decimal  "saturated_fats"
    t.decimal  "carbohydrates"
    t.decimal  "sugars"
    t.decimal  "fiber"
    t.decimal  "proteins"
    t.decimal  "salt"
    t.decimal  "vitamin_a"
    t.decimal  "vitamin_b1"
    t.decimal  "vitamin_b2"
    t.decimal  "vitamin_b3"
    t.decimal  "vitamin_b5"
    t.decimal  "vitamin_b6"
    t.decimal  "vitamin_b7"
    t.decimal  "vitamin_b9"
    t.decimal  "vitamin_b12"
    t.decimal  "vitamin_c"
    t.decimal  "vitamin_d"
    t.decimal  "vitamin_e"
    t.decimal  "vitamin_k"
    t.decimal  "nutrition_per"
    t.string   "nutrition_per_unit"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["product_id"], name: "index_product_nutritions_on_product_id", using: :btree
  end

  create_table "product_shops", force: :cascade do |t|
    t.integer  "product_id"
    t.string   "barcode"
    t.decimal  "amount"
    t.text     "amount_unit"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["product_id"], name: "index_product_shops_on_product_id", using: :btree
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_products_on_product_id", using: :btree
  end

  create_table "recipe_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recipe_ingredients", force: :cascade do |t|
    t.integer  "recipe_id"
    t.string   "quantity"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_recipe_ingredients_on_product_id", using: :btree
    t.index ["recipe_id"], name: "index_recipe_ingredients_on_recipe_id", using: :btree
  end

  create_table "recipe_steps", force: :cascade do |t|
    t.integer  "step_number"
    t.text     "description"
    t.string   "image"
    t.integer  "recipe_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["recipe_id"], name: "index_recipe_steps_on_recipe_id", using: :btree
  end

  create_table "recipes", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.integer  "product_id"
    t.integer  "cuisine_id"
    t.string   "cooking_method"
    t.string   "cooking_time"
    t.string   "preperation_time"
    t.string   "amount_of_servings"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "recipe_category_id"
    t.index ["cuisine_id"], name: "index_recipes_on_cuisine_id", using: :btree
    t.index ["product_id"], name: "index_recipes_on_product_id", using: :btree
    t.index ["recipe_category_id"], name: "index_recipes_on_recipe_category_id", using: :btree
  end

  create_table "shop_franchises", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "website"
  end

  add_foreign_key "product_in_shop_franchises", "products"
  add_foreign_key "product_in_shop_franchises", "shop_franchises"
  add_foreign_key "product_nutritions", "products"
  add_foreign_key "product_shops", "products"
  add_foreign_key "products", "products"
  add_foreign_key "recipe_ingredients", "products"
  add_foreign_key "recipe_ingredients", "recipes"
  add_foreign_key "recipe_steps", "recipes"
  add_foreign_key "recipes", "cuisines"
  add_foreign_key "recipes", "products"
  add_foreign_key "recipes", "recipe_categories"
end
