# Seeds the application
# To quickly test/reset (in a testing environment): `rake db:reset`
# Make sure the server is closed first! Otherwise changes will not come
# through.

cuisines = Cuisine.create([
	{name: 'french'},
	{name: 'italian'},
	{name: 'belgian'},
	{name: 'spanish'},
	{name: 'thai'},
	{name: 'indian'},
	{name: 'mexican'},
	{name: 'asian'},
	{name: 'indonesian'},
	{name: 'turkish'}
])

categories = RecipeCategory.create([
	{name: 'dinner'},
	{name: 'lunch'},
	{name: 'breakfast'},
	{name: 'drink'},
	{name: 'snacks'},
	{name: 'soups'}
])

pesto = Product.create({name: 'Pesto'})
red_pesto = Product.create({name: 'Red Pesto', parent: pesto})
green_pesto = Product.create({name: 'Green Pesto', parent: pesto})
homemade_pesto = Product.create({name: 'Homemade Pesto', parent: green_pesto})
pesto_boni = Product.create({name: 'Boni Groene Pesto', parent: green_pesto})

salt = Product.create({name: 'Salt'})
black_pepper = Product.create({name: 'Black Pepper'})
herbs = Product.create({name: 'Herbs'})
oregano = Product.create({name: 'Oregano', parent: oregano})
basil_leaves = Product.create({name: 'Basil Leaves', parent: herbs})
garlic = Product.create({name: 'Garlic'})

cheese = Product.create({name: 'Cheese'})
cheese_parmezan = Product.create({name: 'Parmesan', parent: cheese})
cheese_mozzarella = Product.create({name: 'Mozzarella Cheese', parent: cheese})
cheese_gouda = Product.create({name: 'Goude Cheese', parent: cheese})
cheese_feta = Product.create({name: 'Feta', parent: cheese})

pine_nuts = Product.create({name: 'Pine Nuts'})

butter = Product.create({name: 'Butter'})
oil = Product.create({name: 'Cooking oil'})
olive_oil = Product.create({name: 'Olive Oil', parent: oil})
olive_oil_extra = Product.create({name: 'Extra virgin olive oil', parent: olive_oil})

tuna_fish = Product.create({name: 'Tuna Fish'})
salmon = Product.create({name: 'Salmon'})
throut = Product.create({name: 'Throut'})

tomato = Product.create({name: 'Tomato'})
bell_pepper = Product.create({name: 'Bell pepper'})
red_bell_pepper = Product.create({name: 'Red Bell pepper', parent: bell_pepper})
green_bell_pepper = Product.create({name: 'Green Bell pepper', parent: bell_pepper})
orange_bell_pepper = Product.create({name: 'Orange Bell pepper', parent: bell_pepper})
lettuce = Product.create({name: 'Lettuce'})
cucumber = Product.create({name: 'Cucumber'})
brocolli = Product.create({name: 'Brocolli'})
carrots = Product.create({name: 'Carrots'})
potato = Product.create({name: 'Potato'})
tomato_cubes = Product.create({name: 'Tomato cubes'})
tomato_paste = Product.create({name: 'Tomato paste'})
tomato_sauce = Product.create({name: 'Tomato sauce'})
onion = Product.create({name: 'Onion'})
eggplant = Product.create({name: 'Eggplant'})
zucchini = Product.create({name: 'Zucchini'})

pizza_crust = Product.create({name: 'Pizza Crust'})
pizza_margherita = Product.create({name: 'Pizza Margherita'})

bananas = Product.create({name: 'Bananas'})
pineapple = Product.create({name: 'Pineapple'}) # Or is it ananas (?)
lemon = Product.create({name: 'Lemon'})
lime = Product.create({name: 'Lime'})
orange = Product.create({name: 'Orange'})
grape = Product.create({name: 'Grape'})
red_grapes = Product.create({name: 'Red Grapes'})
blue_grapes = Product.create({name: 'Blue Grapes'})
clementine = Product.create({name: 'Clementine'})
strawberry = Product.create({name: 'Strawberry'})
blackberry = Product.create({name: 'Blackberry'})
apple = Product.create({name: 'Apple'})

milk = Product.create({name: 'Milk'})
chocolate_milk = Product.create({name: 'Chocolate Milk'})
shop_milk = Product.create({name: 'Milk from Shoppe', parent: milk})

rice = Product.create({name: 'Rice'})
brown_rice = Product.create({name: 'Brown rice', parent: rice})
white_rice = Product.create({name: 'White rice', parent: rice})
long_grain_rice = Product.create({name: 'Long Grain Rice', parent: white_rice})

stuffed_bell_peppers = Product.create({name: 'Stuffed Bell Peppers'})

panini_bread = Product.create({name: 'Panini bread'})
panini_moz_pesto = Product.create({name: 'Panini Mozzarella Pesto'})

shop_milk_nutrition = ProductNutrition.create({
	product: shop_milk,
	calories: 45.0,
	fats: 1.6,
	saturated_fats: 1.0,
	carbohydrates: 4.6,
	sugars: 4.6,
	proteins: 3.3,
	salt: 0.1,
	nutrition_per: 100.0,
	nutrition_per_unit: 'ml'
})

shop_milk_shopinfo = ProductShop.create({
	product: shop_milk,
	barcode: '27006023',
	amount: 1.0,
	amount_unit: 'l'
})

# Source: http://www.simplyrecipes.com/recipes/fresh_basil_pesto/
pesto_recipe = Recipe.create({
	name: 'Fresh Basil Pesto',
	description: 'Follow this easy recipe to create your own homemade pesto.',
	product: homemade_pesto,
	cuisine: cuisines[1],
	cooking_method: 'mashing',
	cooking_time: '10m',
	preperation_time: '5m',
	amount_of_servings: 1,
	image: 'http://assets.simplyrecipes.com/wp-content/uploads/2014/08/basil-pesto-vertical-a-640.jpg'
})

RecipeIngredient.create([
	{recipe: pesto_recipe, product: basil_leaves, quantity: '2 cups'},
	{recipe: pesto_recipe, product: cheese_parmezan, quantity: '1/2 cup'},
	{recipe: pesto_recipe, product: olive_oil_extra, quantity: '1/2 cup'},
	{recipe: pesto_recipe, product: pine_nuts, quantity: '1/3 cup'},
	{recipe: pesto_recipe, product: garlic, quantity: '3 cloves'},
	{recipe: pesto_recipe, product: salt, quantity: 'to taste'},
	{recipe: pesto_recipe, product: black_pepper, quantity: 'to taste'},
])

RecipeStep.create([
	{recipe: pesto_recipe, step_number: 1, description: 'Place the basil leaves and pine nuts into the bowl of a food processor and pulse a several times.', image: 'http://assets.simplyrecipes.com/wp-content/uploads/2014/08/basil-pesto-method-1.jpg'},
	{recipe: pesto_recipe, step_number: 2, description: 'Add the garlic and Parmesan or Romano cheese and pulse several times more. Scrape down the sides of the food processor with a rubber spatula.', image: 'http://assets.simplyrecipes.com/wp-content/uploads/2014/08/basil-pesto-method-3.jpg'},
	{recipe: pesto_recipe, step_number: 3, description: 'While the food processor is running, slowly add the olive oil in a steady small stream. Adding the olive oil slowly, while the processor is running, will help it emulsify and help keep the olive oil from separating. Occasionally stop to scrape down the sides of the food processor.', image: 'http://assets.simplyrecipes.com/wp-content/uploads/2014/08/basil-pesto-method-5.jpg'},
	{recipe: pesto_recipe, step_number: 4, description: 'Stir in some salt and freshly ground black pepper to taste.', image: 'http://assets.simplyrecipes.com/wp-content/uploads/2014/08/basil-pesto-method-6.jpg'}
])

panini_moz_pesto_recipe = Recipe.create({
	name: 'Panini Mozzarella Pesto',
	description: 'Tasty recipe for a quick snack!',
	product: panini_moz_pesto,
	cuisine: cuisines[1],
	cooking_method: 'grill',
	cooking_time: '10m',
	preperation_time: '5m',
	amount_of_servings: 2,
	recipe_category: categories[4]
})

RecipeIngredient.create([
	{recipe: panini_moz_pesto_recipe, product: pesto, quantity: ''},
	{recipe: panini_moz_pesto_recipe, product: panini_bread, quantity: '2'},
	{recipe: panini_moz_pesto_recipe, product: cheese_mozzarella, quantity: '1'},
	{recipe: panini_moz_pesto_recipe, product: tomato, quantity: '1'}
])

RecipeStep.create([
	{recipe: panini_moz_pesto_recipe, step_number: 1, description: 'Cut the panini bread in half and cover the inside with pesto.'},
	{recipe: panini_moz_pesto_recipe, step_number: 2, description: 'Place mozzarella slides in the bread, together with some sliced tomatoes.'},
	{recipe: panini_moz_pesto_recipe, step_number: 3, description: 'Place it under a grill for a couple of minutes until the bread is slightly brownish.'}
])

# Source: own recipe (Bjarno)
stuffed_bell_peppers_recipe = Recipe.create({
	name: 'Stuffed Bell Peppers',
	description: 'It takes a while to cook this. But the result is delicious!',
	product: stuffed_bell_peppers,
	cuisine: cuisines[2],
	cooking_method: 'oven',
	cooking_time: '70m',
	preperation_time: '20m',
	amount_of_servings: 2,
	recipe_category: categories[0]
})

RecipeIngredient.create([
	{recipe: stuffed_bell_peppers_recipe, product: tomato_cubes, quantity: '250 g'},
	{recipe: stuffed_bell_peppers_recipe, product: onion, quantity: '1'},
	{recipe: stuffed_bell_peppers_recipe, product: eggplant, quantity: '1/4'},
	{recipe: stuffed_bell_peppers_recipe, product: zucchini, quantity: '1/4'},
	{recipe: stuffed_bell_peppers_recipe, product: carrots, quantity: '1'},
	{recipe: stuffed_bell_peppers_recipe, product: oregano, quantity: ''},
	{recipe: stuffed_bell_peppers_recipe, product: black_pepper, quantity: ''},
	{recipe: stuffed_bell_peppers_recipe, product: red_bell_pepper, quantity: '2'},
	{recipe: stuffed_bell_peppers_recipe, product: long_grain_rice, quantity: '1 sachet'},
	{recipe: stuffed_bell_peppers_recipe, product: tomato_paste, quantity: '70 g'},
	{recipe: stuffed_bell_peppers_recipe, product: cheese_feta, quantity: '70 g'},
	{recipe: stuffed_bell_peppers_recipe, product: butter, quantity: ''},
])

RecipeStep.create([
	{recipe: stuffed_bell_peppers_recipe, step_number: 1, description: 'Cut the onions, the eggplant and the zucchini as fine as possible. Also slice the carrots.'},
	{recipe: stuffed_bell_peppers_recipe, step_number: 2, description: 'Heat the onions in a casserole (with butter). Add the tomato cubes, the zucchini and the eggplant after the onions are slightly brownish.'},
	{recipe: stuffed_bell_peppers_recipe, step_number: 3, description: 'Add herbs, to taste. This finishes the sauce.'},
	{recipe: stuffed_bell_peppers_recipe, step_number: 4, description: 'Make sure everything in the casserole is heated properly before continuing with the bell peppers themselves.'},
	{recipe: stuffed_bell_peppers_recipe, step_number: 5, description: 'Cook the rice, following the instructions on the box.'},
	{recipe: stuffed_bell_peppers_recipe, step_number: 6, description: 'Cut the feta cheese in large blocks, and add it together with the tomato paste and rice in a big bowl.'},
	{recipe: stuffed_bell_peppers_recipe, step_number: 7, description: 'Preheat the oven at 160 degrees celsius.'},
	{recipe: stuffed_bell_peppers_recipe, step_number: 8, description: 'Cut the bell peppers in half, and fill them with the rice/feta/tomato paste combinition.'},
	{recipe: stuffed_bell_peppers_recipe, step_number: 9, description: 'Take an oven dish and fill it with the earlier prepared sauce.'},
	{recipe: stuffed_bell_peppers_recipe, step_number: 10, description: 'Now place the peppers in the dish as well, so they are soaked with the sauce.'},
	{recipe: stuffed_bell_peppers_recipe, step_number: 11, description: 'Cover the dish with the aluminium foil, and put it into the oven for 60 minutes.'}
])

# Source: http://sallysbakingaddiction.com/2014/07/15/classic-margherita-pizza/
pizza_margherita_recipe = Recipe.create({
	name: 'Pizza Margherita',
	description: 'The classic pizza from Italy!',
	product: pizza_margherita,
	cuisine: cuisines[1],
	cooking_method: 'oven',
	cooking_time: '25m',
	preperation_time: '15m',
	amount_of_servings: 1,
	recipe_category: categories[0]
})

RecipeIngredient.create([
	{recipe: pizza_margherita_recipe, product: pizza_crust, quantity: '1'},
	{recipe: pizza_margherita_recipe, product: olive_oil, quantity: '1 tablespoon'},
	{recipe: pizza_margherita_recipe, product: garlic, quantity: '1'},
	{recipe: pizza_margherita_recipe, product: tomato_sauce, quantity: '1/4 cup'},
	{recipe: pizza_margherita_recipe, product: cheese_mozzarella, quantity: '8 ounces'},
	{recipe: pizza_margherita_recipe, product: tomato, quantity: '2'},
	{recipe: pizza_margherita_recipe, product: basil_leaves, quantity: ''},
	{recipe: pizza_margherita_recipe, product: black_pepper, quantity: 'to taste'}
])

RecipeStep.create([
	{recipe: pizza_margherita_recipe, step_number: 1, description: 'Preheat the oven to 180 degrees.'},
	{recipe: pizza_margherita_recipe, step_number: 1, description: 'Mix the olive oil and chopped garlic together in a small dish. After waiting 15 minutes, spread olive oil/garlic mixture on top of crust. Top with pizza sauce, then the mozzarella cheese slices, then the tomato slices.'},
	{recipe: pizza_margherita_recipe, step_number: 1, description: 'Bake for 14-16 minutes or until the crust is lightly browned and the cheese is bubbling.'},
	{recipe: pizza_margherita_recipe, step_number: 1, description: 'Remove from the oven and top with fresh basil and pepper. Slice pizza and serve immediately.'}
])


# Shop descriptions from Wikipedia :)

franchise_colruyt = ShopFranchise.create({
	name: 'Colruyt',
	description: 'Colruyt is a chain of supermarkets, started at Lembeek near Halle, Belgium. Colruyt carries the name of the founder of the family business: Franz Colruyt. Colruyt is part of Colruyt Group, a Belgian multinational family business in wholesale distribution that evolved from the Colruyt supermarkets. It is competing with hard discounters such as Aldi and Lidl in Benelux countries where it is a well established market leader. The current manager is Jef Colruyt.',
	image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Colruyt_logo.png/200px-Colruyt_logo.png',
	website: 'https://www.colruyt.be/'
})

franchise_aldi = ShopFranchise.create({
	name: 'Aldi',
	description: 'Aldi (stylized as ALDI) is a leading global discount supermarket chain with over 10,000 stores in 18 countries, and an estimated turnover of more than €50 billion. Based in Germany, the chain was founded by brothers Karl and Theo Albrecht in 1946 when they took over their mother\'s store in Essen which had been in operation since 1913; it is one of the world\'s largest privately owned companies.',
	image: 'https://upload.wikimedia.org/wikipedia/commons/2/20/ALDI_Nord_Logo_2015.png',
	website: 'https://aldi.be/'
})

franchise_delhaize = ShopFranchise.create({
	name: 'Delhaize',
	description: 'Delhaize Le Lion / De Leeuw (French pronunciation: ​[dəlɛːz]) was a food retailer headquartered in Sint-Jans-Molenbeek, Brussels, Belgium, and operating in seven countries and on three continents. The principal activity of Delhaize Group is the operation of food supermarkets. On June 24, 2015, Delhaize reached an agreement with Ahold to merge and form a new parent company headquartered in the Netherlands: Ahold Delhaize.',
	image: 'http://www.therobinreport.com/wp-content/uploads/2015/09/logo-logo_delhaize_67.jpg',
	website: 'http://delhaize.be/'
})

franchise_lidl = ShopFranchise.create({
	name: 'Lidl',
	description: 'Lidl Stiftung & Co. KG (German pronunciation: [ˈliːdəl]; UK /ˈlɪdəl/ LID-əl), formerly Schwarz Unternehmens Treuhand KG, is a German global discount supermarket chain, based in Neckarsulm, Baden-Württemberg, Germany, that operates over 10,000 stores across Europe. It belongs to the holding company Schwarz Gruppe, which also owns the store chains Handelshof and hypermarket Kaufland.',
	image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Lidl-Logo.svg/500px-Lidl-Logo.svg.png',
	website: 'http://www.lidl.be/'
})

franchise_carrefour = ShopFranchise.create({
	name: 'Carrefour',
	description: 'Carrefour S.A. (French pronunciation: ​[kaʁfuʁ]) is a French multinational retailer headquartered in Boulogne Billancourt, France, in the Hauts-de-Seine Department near Paris. It is one of the largest hypermarket chains in the world (with close to 1,600 hypermarkets at the end of 2015), the Second largest retail group in the world in terms of revenue (after Walmart, and the second in profit (after Walmart). Carrefour operates in more than 30 countries, in Europe, the Americas, Asia and Africa. Carrefour means "crossroads" and "public square" in French. The company is a component of the Euro Stoxx 50 stock market index.',
	image: 'https://upload.wikimedia.org/wikipedia/commons/0/01/Logo_Carrefour_sans_texte.png',
	website: 'http://carrefour.eu/'
})

franchise_ah = ShopFranchise.create({
	name: 'Albert Heijn',
	description: 'Albert Heijn B.V. is the largest Dutch supermarket chain, founded in 1887 in Oostzaan, Netherlands. It is named after Albert Heijn, Sr., the founder of the first store in Oostzaan.',
	image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Albert_Heijn_Logo.svg/500px-Albert_Heijn_Logo.svg.png',
	website: 'http://www.ah.be/'
})

ProductInShopFranchise.create({
	product: shop_milk,
	shop_franchise: franchise_lidl,
	price: 1.75,
	price_date: '2016-12-06'
})