require 'test_helper'

class ShopFranchisesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @shop_franchise = shop_franchises(:one)
  end

  test "should get index" do
    get shop_franchises_url
    assert_response :success
  end

  test "should get new" do
    get new_shop_franchise_url
    assert_response :success
  end

  test "should create shop_franchise" do
    assert_difference('ShopFranchise.count') do
      post shop_franchises_url, params: { shop_franchise: { description: @shop_franchise.description, image: @shop_franchise.image, name: @shop_franchise.name } }
    end

    assert_redirected_to shop_franchise_url(ShopFranchise.last)
  end

  test "should show shop_franchise" do
    get shop_franchise_url(@shop_franchise)
    assert_response :success
  end

  test "should get edit" do
    get edit_shop_franchise_url(@shop_franchise)
    assert_response :success
  end

  test "should update shop_franchise" do
    patch shop_franchise_url(@shop_franchise), params: { shop_franchise: { description: @shop_franchise.description, image: @shop_franchise.image, name: @shop_franchise.name } }
    assert_redirected_to shop_franchise_url(@shop_franchise)
  end

  test "should destroy shop_franchise" do
    assert_difference('ShopFranchise.count', -1) do
      delete shop_franchise_url(@shop_franchise)
    end

    assert_redirected_to shop_franchises_url
  end
end
