class RecipeCategoriesController < ApplicationController

	def index
		@categories = RecipeCategory.all
	end

	def show
		@category = RecipeCategory.find(params[:id])
		@recipes = @category.recipes.all
	end

	def new
		@category = RecipeCategory.new
	end

	def create
		@category = RecipeCategory.new(category_params)

		if @category.save
			redirect_to @category
		else
			render 'new'
		end
	end

	def edit
		@category = RecipeCategory.find(params[:id])
	end

	def update
		@category = RecipeCategory.find(params[:id])

		if @category.update(category_params)
			redirect_to @category
		else
			render 'edit'
		end
	end

	def destroy
		@category = RecipeCategory.find(params[:id])
		@category.destroy
		
		redirect_to recipe_categories_path
	end

	private
	def category_params
		params.require(:recipe_category).permit(:name)
	end
end