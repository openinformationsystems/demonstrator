class RecipeIngredientsController < ApplicationController

	def new
		@recipe = Recipe.find(params[:recipe_id])
    	@recipe_ingredient = RecipeIngredient.new()
	end

	def create
		@recipe = Recipe.find(params[:recipe_id])

		@recipe_ingredient = RecipeIngredient.new(recipe_ingredient_params)
		associate_product @recipe_ingredient
		@recipe_ingredient.recipe = @recipe

		if @recipe_ingredient.save
			redirect_to @recipe
		else
			render 'new'
		end
	end

	def edit
		@recipe = Recipe.find(params[:recipe_id])
		@recipe_ingredient = @recipe.recipe_ingredients.find(params[:id])
	end

	def update
		@recipe = Recipe.find(params[:recipe_id])
		@recipe_ingredient = @recipe.recipe_ingredients.find(params[:id])

		if @recipe_ingredient.update(recipe_ingredient_params)
			redirect_to @recipe
		else
			render 'edit'
		end
	end

	def destroy
		@recipe = Recipe.find(params[:recipe_id])
		@recipe_ingredient = @recipe.recipe_ingredients.find(params[:id])

		@recipe_ingredient.destroy
		
		redirect_to recipe_path(@recipe)
	end

	private
	def recipe_ingredient_params
		params.require(:recipe_ingredient).permit(:quantity)
	end

	def associate_product recipe_ingredient
		if (params['recipe_ingredient']['product'].length > 0)
			product_id = params['recipe_ingredient']['product'].to_i
			product = Product.find(product_id)

			recipe_ingredient.product = product
		end
	end

end