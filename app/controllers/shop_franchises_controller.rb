class ShopFranchisesController < ApplicationController
  before_action :set_shop_franchise, only: [:show, :edit, :update, :destroy]

  # GET /shop_franchises
  # GET /shop_franchises.json
  def index
    @shop_franchises = ShopFranchise.all
  end

  # GET /shop_franchises/1
  # GET /shop_franchises/1.json
  def show
  end

  # GET /shop_franchises/new
  def new
    @shop_franchise = ShopFranchise.new
  end

  # GET /shop_franchises/1/edit
  def edit
  end

  # POST /shop_franchises
  # POST /shop_franchises.json
  def create
    @shop_franchise = ShopFranchise.new(shop_franchise_params)

    respond_to do |format|
      if @shop_franchise.save
        format.html { redirect_to @shop_franchise, notice: 'Shop franchise was successfully created.' }
        format.json { render :show, status: :created, location: @shop_franchise }
      else
        format.html { render :new }
        format.json { render json: @shop_franchise.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shop_franchises/1
  # PATCH/PUT /shop_franchises/1.json
  def update
    respond_to do |format|
      if @shop_franchise.update(shop_franchise_params)
        format.html { redirect_to @shop_franchise, notice: 'Shop franchise was successfully updated.' }
        format.json { render :show, status: :ok, location: @shop_franchise }
      else
        format.html { render :edit }
        format.json { render json: @shop_franchise.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shop_franchises/1
  # DELETE /shop_franchises/1.json
  def destroy
    @shop_franchise.destroy
    respond_to do |format|
      format.html { redirect_to shop_franchises_url, notice: 'Shop franchise was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shop_franchise
      @shop_franchise = ShopFranchise.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shop_franchise_params
      params.require(:shop_franchise).permit(:name, :description, :image, :website)
    end
end
