class RecipesController < ApplicationController

	def index
		@recipes = Recipe.all
	end

	def show
		@recipe = Recipe.find(params[:id])
	end

	def new
		@recipe = Recipe.new
	end

	def create
		@recipe = Recipe.new(recipe_params)
		fix_assocs @recipe

		if @recipe.save
			redirect_to @recipe
		else
			render 'new'
		end
	end

	def edit
		@recipe = Recipe.find(params[:id])
	end

	def update
		@recipe = Recipe.find(params[:id])
		fix_assocs @recipe

		if @recipe.update(recipe_params)
			redirect_to @recipe
		else
			render 'edit'
		end
	end

	def destroy
		@recipe = Recipe.find(params[:id])
		@recipe.destroy
		
		redirect_to recipes_path
	end

	private
	def recipe_params
		params.require(:recipe).permit(:name, :description, :image, :cooking_method, :preperation_time, :cooking_time, :amount_of_servings)
	end

	def fix_assocs recipe
		associate_cuisine recipe
		associate_product recipe
		associate_category recipe
	end

	def associate_cuisine recipe
		if (params['recipe']['cuisine'].length > 0)
			cuisine_id = params['recipe']['cuisine'].to_i
			product = Cuisine.find(cuisine_id)

			recipe.cuisine = product
		end
	end

	def associate_product recipe
		if (params['recipe']['product'].length > 0)
			product_id = params['recipe']['product'].to_i
			product = Product.find(product_id)

			recipe.product = product
		end
	end

	def associate_category recipe
		if (params['recipe']['recipe_category'].length > 0)
			category_id = params['recipe']['recipe_category'].to_i
			category = RecipeCategory.find(category_id)

			recipe.recipe_category = category
		end
	end

end