class RecipeStepsController < ApplicationController

	def index

	end

	def new
		@recipe = Recipe.find(params[:recipe_id])
    	@recipe_step = RecipeStep.new()
	end

	def create
		@recipe = Recipe.find(params[:recipe_id])

		@recipe_step = RecipeStep.new(recipe_step_params)
		@recipe_step.recipe = @recipe

		if @recipe_step.save
			redirect_to @recipe
		else
			render 'new'
		end
	end

	def edit
		@recipe = Recipe.find(params[:recipe_id])
		@recipe_step = @recipe.recipe_steps.find(params[:id])
	end


	def update
		@recipe = Recipe.find(params[:recipe_id])
		@recipe_step = @recipe.recipe_steps.find(params[:id])

		if @recipe_step.update(recipe_step_params)
			redirect_to @recipe
		else
			render 'edit'
		end
	end
	
	def destroy
		@recipe = Recipe.find(params[:recipe_id])
		@recipe_step = @recipe.recipe_steps.find(params[:id])

		@recipe_step.destroy
		
		redirect_to recipe_path(@recipe)
	end

	private
	def recipe_step_params
		params.require(:recipe_step).permit(:step_number, :description, :image)
	end

end