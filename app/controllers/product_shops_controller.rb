class ProductShopsController < ApplicationController

	def index
		@product = Product.find(params[:product_id])
		@info_shop = @product.product_shop

		if @info_shop.nil?
			raise ActionController::RoutingError.new('No shop information found for product')
		end
	end

	def new
		@product = Product.find(params[:product_id])
		@info_shop = ProductShop.new
	end

	def create
		@product = Product.find(params[:product_id])
		@info_shop = ProductShop.new(shop_params)
		@info_shop.product = @product

		if @info_shop.save
			redirect_to @product
		else
			render 'new'
		end
	end

	def edit
		@product = Product.find(params[:product_id])
		@info_shop = ProductShop.find(params[:id])
	end

	def update
		@product = Product.find(params[:product_id])
		@info_shop = ProductShop.find(params[:id])

		if @info_shop.update(shop_params)
			redirect_to @product
		else
			render 'edit'
		end
	end

	def destroy
		@product = Product.find(params[:product_id])
		@info_shop = ProductShop.find(params[:id])
		
		@info_shop.destroy

		redirect_to product_path @product
	end

	private
	def shop_params
		params.require(:product_shop).permit(:barcode, :amount, :amount_unit)
	end

end