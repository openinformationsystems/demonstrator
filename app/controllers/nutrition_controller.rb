class NutritionController < ApplicationController

	def index
		@product = Product.find(params[:product_id])
		@nutrition = @product.product_nutrition

		if @nutrition.nil?
			raise ActionController::RoutingError.new('No nutritional information found')
		end
	end

	def new
		@product = Product.find(params[:product_id])
		@nutrition = ProductNutrition.new
	end

	def create
		@product = Product.find(params[:product_id])
		@nutrition = ProductNutrition.new(nutrition_params)
		@nutrition.product = @product

		if @nutrition.save
			redirect_to @product
		else
			render 'new'
		end
	end

	def edit
		@product = Product.find(params[:product_id])
		@nutrition = ProductNutrition.find(params[:id])
	end

	def update
		@product = Product.find(params[:product_id])
		@nutrition = ProductNutrition.find(params[:id])

		if @product.update(product_params)
			redirect_to @product
		else
			render 'edit'
		end
	end

	def destroy
		@product = Product.find(params[:product_id])
		@nutrition = ProductNutrition.find(params[:id])
		
		@nutrition.destroy

		redirect_to product_path @product
	end

	private
	def nutrition_params
		params.require(:product_nutrition).permit(:calories, :fats, :saturated_fats, :carbohydrates, :sugars, :fiber, :proteins, :salt, :vitamin_a, :vitamin_b1, :vitamin_b2, :vitamin_b3, :vitamin_b5, :vitamin_b6, :vitamin_b7, :vitamin_b9, :vitamin_b12, :vitamin_c, :vitamin_d, :vitamin_e, :vitamin_k, :nutrition_per, :nutrition_per_unit)
	end

end