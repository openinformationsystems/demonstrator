class ProductsController < ApplicationController

	def index
		@products = Product.all
	end

	def show
		@product = Product.find(params[:id])
		@nutrition = @product.product_nutrition
		@shop_info = @product.product_shop
		@product_in_shop = ProductInShopFranchise.new

		recipe_usages = @product.recipe_ingredients.all
		@recipes = []
		recipe_usages.each do |item|
			@recipes.push(item.recipe)
		end

		@creating_recipes = @product.recipes

		@children = @product.child_products.all

	end

	def show_shop_availability
		@product = Product.find(params[:id])
		@product_in_shop = ProductInShopFranchise.new
	end

	def new
		@product = Product.new
	end

	def create
		@product = Product.new(product_params)
		set_product_parent @product

		if @product.save
			redirect_to @product
		else
			render 'new'
		end
	end

	def edit
		@product = Product.find(params[:id])
	end

	def update
		@product = Product.find(params[:id])
		set_product_parent @product

		if @product.update(product_params)
			redirect_to @product
		else
			render 'edit'
		end
	end

	def destroy
		@product = Product.find(params[:id])
		@product.destroy
		
		redirect_to products_path
	end

	private
	def product_params
		params.require(:product).permit(:name)
	end

	def set_product_parent product
		if (params['product']['parent'].length > 0)
			parent_id = params['product']['parent'].to_i

			parent = Product.find_by_id(parent_id)

			if !(parent.nil?)
				product.parent = parent
			end
		end
	end

end