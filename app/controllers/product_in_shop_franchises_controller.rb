class ProductInShopFranchisesController < ApplicationController

  def create
    @product = Product.find(params[:product_id])
    @product_in_shop = @product.product_in_shop_franchise.create(product_in_shop_franchises_params)
    set_franchise @product_in_shop

    @product_in_shop.save

    redirect_to product_path(@product) + '/shop_availability'
  end

  def edit
    @product = Product.find(params[:product_id])
    @product_in_shop = @product.product_in_shop_franchise.find(params[:id])
  end

  def update
    @product = Product.find(params[:product_id])
    @product_in_shop = @product.product_in_shop_franchise.find(params[:id])

    set_franchise @product_in_shop

    if @product_in_shop.update(product_in_shop_franchises_params)
      redirect_to product_path(@product) + '/shop_availability'
    else
      render 'edit'
    end
  end

  def destroy
    @product = Product.find(params[:product_id])
    @product_in_shop = @product.product_in_shop_franchise.find(params[:id])
    @product_in_shop.destroy
    redirect_to product_path(@product) + '/shop_availability'
  end
 
  private
    def product_in_shop_franchises_params
      params.require(:product_in_shop_franchise).permit(:price, :price_date)
    end

    def set_franchise details
		if (params['product_in_shop_franchise']['shop_franchise'].length > 0)
			parent_id = params['product_in_shop_franchise']['shop_franchise'].to_i

			franchise = ShopFranchise.find_by_id(parent_id)

			if !(franchise.nil?)
				details.shop_franchise = franchise
			end
		end
	end
end