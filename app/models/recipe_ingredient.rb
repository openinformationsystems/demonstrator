class RecipeIngredient < ApplicationRecord
  belongs_to :recipe, :foreign_key => "recipe_id"
  belongs_to :product, :foreign_key => "product_id"
end
