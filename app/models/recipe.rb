class Recipe < ApplicationRecord
  belongs_to :product, optional: true
  belongs_to :cuisine, optional: true
  belongs_to :recipe_category, optional: true
  has_many :recipe_steps
  has_many :recipe_ingredients
end
