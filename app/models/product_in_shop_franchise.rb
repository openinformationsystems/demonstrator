class ProductInShopFranchise < ApplicationRecord
  belongs_to :product
  belongs_to :shop_franchise
end
