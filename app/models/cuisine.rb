class Cuisine < ApplicationRecord
	has_many :recipes, :class_name => "Recipe", :foreign_key => "cuisine_id"
end
