class RecipeCategory < ApplicationRecord
	has_many :recipes, :class_name => "Recipe", :foreign_key => "recipe_category_id"
end
