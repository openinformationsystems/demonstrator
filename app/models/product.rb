class Product < ApplicationRecord
  belongs_to :parent, :class_name => "Product", :foreign_key => "product_id", optional: true
  has_many :child_products, :class_name => "Product", :foreign_key => "product_id"
  has_one :product_nutrition
  has_one :product_shop
  has_many :product_in_shop_franchise, :class_name => "ProductInShopFranchise", :foreign_key => "product_id"
  has_many :recipe_ingredients, :class_name => "RecipeIngredient", :foreign_key => "product_id"
  has_many :recipes, :class_name => "Recipe", :foreign_key => "product_id"
end