class ShopFranchise < ApplicationRecord
	has_many :product_in_shop_franchise, :class_name => "ProductInShopFranchise", :foreign_key => "shop_franchise"
end