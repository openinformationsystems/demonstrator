json.extract! shop_franchise, :id, :name, :description, :image, :created_at, :updated_at
json.url shop_franchise_url(shop_franchise, format: :json)