# Food Food Food

This is our project's demonstrator for the Open Information Systems course at the VUB (http://vub.ac.be).

# Set-up

Dependencies : `ruby`, `gem`, `sqlite` headers.

    # gem install bundle
    $ bundle install
    $ rake db:migrate
    $ rake db:seed
    $ rails s

The server should now run on http://localhost:3000


# D2R Server

    $ DBUSER=my-user DBPASS=my-pass DBNAME=my-db-name make run

Then you should have a web interface at http://localhost:2020/
