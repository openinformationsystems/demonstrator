d2rq-bin:
	rm -f d2rq-0.8.1.zip
	wget -q https://github.com/downloads/d2rq/d2rq/d2rq-0.8.1.zip
	unzip  d2rq-0.8.1.zip
	rm d2rq-0.8.1.zip
	mv d2rq-0.8.1 d2rq-bin
	chmod +x ./d2rq-bin/generate-mapping ./d2rq-bin/d2r-server

mapping.ttl: d2rq-bin
	./d2rq-bin/generate-mapping \
	-o mapping.ttl \
	--skip-tables ar_internal_metadata,schema_migrations \
	-d org.postgresql.Driver \
	-u $(DBUSER) -p $(DBPASS) \
	jdbc:postgresql:$(DBNAME)

run: mapping.ttl
	cd d2rq-bin; ./d2r-server ../mapping.ttl
